# -*- coding: utf-8 -*-
"""
Created on Tue Feb 20 11:31:45 2018

@author: cal001
"""

from __future__ import print_function
import numpy as np
import csv

def readValues(val_file):
    '''
    TODO
    '''
    vals = {}
    
    v_file = open(val_file, 'r')
    v_lines = v_file.readlines()
    v_file.close()
    
    for line in v_lines:
        if any(char.isdigit() for char in line.split()[0]):
            vals[str(line.split()[1])] = float(line.split()[0])
                    
        elif "C" in line.split()[0] or "F" in line.split()[0] :
            vname = "condition"
            vals[vname] = line.split()[0]
    
    return vals  

def calcFixed(hn,dx,Q,C,b,I,Nx):
    '''
    TODO
    '''
    D = int(dx*Nx)+int(dx) 
    distance = range(int(dx), D, int(dx)) 
    waterdepth = [hn,]
    hg = []
    waterlevel = [hn,]
    
    for i, dis in enumerate(distance):
        val = round(hn + dx * (Q**2.0/((C**2.0)*(hn**3.0)*(b**2.0)) - I ), 3)
        waterdepth.append(val)
        hn = val
        
        hgn = round(dis * I, 3)
        hg.append(hgn)
        
        wl = round(val + hgn, 3)
        waterlevel.append(wl)   
    
    last = round((distance[-1]+dx) * I, 3)
    hg.append(last)
    
    distance = range(0, D, int(dx))
    return waterdepth,hg,waterlevel,distance

def calcConvergence(hn,dx,Q,C,b,I):
    i=1 # point location number 

    # calculate normal depth
    h_norm = round(( Q/( C*b* np.sqrt(I) ) )**(2.0/3.0), 4)
    diff = 1
    
    # calculate until convergence  
    while diff > 0.001:
        val = round(hn + dx * (Q**2.0/((C**2.0)*(hn**3.0)*(b**2.0)) - I ), 4)
        waterdepth.append(val)
        hn = val
        
        hgn = round(dx * i * I, 4)
        i+=1
        hg.append(hgn)
        
        wl = round(val + hgn, 4)
        waterlevel.append(wl)
        
        diff = round(np.abs(h_norm - hn), 4)
        
    n = len(waterlevel)
    D = int(dx*n)
    distance = range(0, D, int(dx))
    hgn = round(dx * i * I, 4)
    hg.append(hgn)
    
    distance = range(0, D, int(dx))

    return waterdepth,hg,waterlevel,distance

def createOutput(R, output_file):
    '''
    TODO
    '''
    
    s = " Backwater Curve \n"
    # Open the file (fname) in write mode, using the with statement
    #qfile_name = raw_input("\nEnter filename to store result matrix:")
    head = ["Point", "Distance", "Water_Depth","Geo_height", "Piezo_height"]
    with open(output_file, 'w') as fname:
        fname.writelines(s)
        writer = csv.writer(fname, delimiter=";")
        writer.writerow(head)
        writer.writerows(R)

pfile_name = 'myapp/static/data/input.txt'
dic = readValues(pfile_name)

# assign each value to the key
for k,v in dic.items():     
    exec("%s=%r" % (k,v))

hn = h0    
waterdepth = [hn,]
hg = []
waterlevel = [hn,]

if str(condition) == "C":
    waterdepth,hg,waterlevel,distance = calcConvergence(hn,dx,Q,C,b,I)

if str(condition) == "F":
    waterdepth,hg,waterlevel,distance = calcFixed(hn,dx,Q,C,b,I,Nx)


R = np.zeros([len(waterdepth),5])
R[:,0] = range(len(waterdepth))
R[:,1] = distance
R[:,2] = waterdepth
R[:,3] = hg
R[:,4] = waterlevel