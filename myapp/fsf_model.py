# -*- coding: utf-8 -*-
"""
Created on Mon Feb 12 14:05:45 2018

@author: tah001
"""
import numpy as np
import matplotlib.pyplot as plt
from scipy import interpolate
from numpy.linalg import inv


## read the text file (file should be in the same location of Jupiter notebook with name Fsf.txt)
#with open('Fsf.txt', 'r') as f:   # Open the file Fsf.txt
#    value = []
#    i = 1 
#    ex = [1, 2, 10, 20, 23]       # list of line that need to skip (exclusion list)
#    for line in f:
#        if i in ex:
#            pass                  # skip the line in the exclusion list
#        else:
#            value.append(line.split()[0])    # split the line based on white space and append only the first value or word
#        i=i+1
#
#
#
## Defining variable names and storing values into the variable
###Numerical Parameters
#dx = int(value[0])          #Space Dicretization (integer data type)
#dt = int(value[1])          #Time step (integer data type)
#TimeMAX = int(value[3])     # Maximum Model Time Run (integer data type)
#NMAXIts = int(value[2])     # Maximum Iterations(integer data type)
#theta = float(value[4])     # Time Coefficient, Preissmann (float data type)
#Psi = float(value[5])       # Space Coefficient, Preissmann(float data type)
#Beta = float(value[6])      # Boussinesq Coefficient(float data type)
###Physical Parameters
#b = int(value[7])          # Storage Width (integer data type)
#C = int(value[8])           # Chezy Coefficient (integer data type)
#g = float(value[9])         # Acceleration due to gravity(float data type)
#Ib = float(value[10])         # Bed Slope (integer data type)
#lgth = int(value[11])       # Length of Channel (integer data type)
#UC = value[12]              # Upstream boundary condition(string data type)
#Ufile = value[13]     # Upstream data file
#DC = value[14]              # Downstream boundary condition(string data type)
#Dfile = value[15]     # Downstream data file
###Data Files
#DepthIn = value[16]         # Name of input water depth file (string data type)
#DischargeIn = value[17]     # Name of input discharge file (string data type)
#DepthOut = value[18]        # Name of output water depth file (string data type)
#DischargeOut = value[19]    # Name of output discharge file (string data type)




def readboundary (Ufile, Dfile, dt, TimeMAX):
    Tfactor = {'days': 86400, 'hours': 3600, 'min': 60, 'sec': 1}
    
    # import upstream boundary data
    with open(Ufile, 'r') as ub:   
        time_ub = []
        data_ub = []
        i = 1 
        for line in ub:
            if i == 2:
                unit = line.split()[0]  
            if i > 3:
                time_ub.append(Tfactor[unit]*float(line.split()[0]))
                data_ub.append(float(line.split()[1]))    # split the line based on white space and append only the first value or word
            i=i+1
            
    # import downstream boundary data
    with open(Dfile, 'r') as db:   
        time_db = []
        data_db = []
        i = 1 
        for line in db:
            if i == 2:
                unit = line.split()[0]  
            if i > 3:
                time_db.append(Tfactor[unit]*float(line.split()[0]))
                data_db.append(float(line.split()[1]))    # split the line based on white space and append only the first value or word
            i=i+1        
            
    f_ub = interpolate.interp1d(time_ub, data_ub)
    f_db = interpolate.interp1d(time_db, data_db)
    timenew = range (0, TimeMAX+1, dt)
    ubc = f_ub(timenew)
    dbc = f_db(timenew)
    return ubc, dbc, timenew



def readini (DepthIn, DischargeIn, dx, lgth):
    Dfactor = {'km': 1000, 'm': 1}
    
    # import initial condition
    with open(DepthIn, 'r') as inidep:   
            dis_ini = []
            value_ini = []
            i = 1 
            for line in inidep:
                if i == 2:
                    unit = line.split()[0]  
                if i > 3:
                    dis_ini.append(Dfactor[unit]*float(line.split()[0]))
                    value_ini.append(float(line.split()[1]))    # split the line based on white space and append only the first value or word
                i=i+1    
    
    # import initial condition
    with open(DischargeIn, 'r') as inidiscrg:   
        dis_iniq = []
        value_iniq = []
        i = 1 
        for line in inidiscrg:
            if i == 2:
                unit = line.split()[0]  
            if i > 3:
                dis_iniq.append(Dfactor[unit]*float(line.split()[0]))
                value_iniq.append(float(line.split()[1]))    # split the line based on white space and append only the first value or word
            i=i+1  
    
    
    f_hini = interpolate.interp1d(dis_ini, value_ini)
    f_qini = interpolate.interp1d(dis_iniq, value_iniq)
    distance = range (0, lgth+1, dx)
    hini = f_hini(distance)
    qini = f_qini(distance)
    return hini, qini, distance




def fsfCalculation (dx, dt, TimeMAX, NMAXIts, theta, Psi, Beta, b, C, g, Ib, lgth, UC, DC, ubc, dbc, hini, qini):    
    M = 1+int(lgth/dx)
    N = 1+int(TimeMAX/dt)
    Q = np.zeros((N, M))
    h = np.zeros((N, M))
    WL = np.zeros((N, M))
    hg = np.zeros((1, M))
    #calculation
    
    E= np.zeros((2*M, 1))
    F= np.zeros((2*M, 2*M))
    
    for x in range (0, M):
        Q[0][x] = qini[x]
        h[0][x] = hini[x]
    

    
    for t in range (1, N):
    
        for i in range (0, NMAXIts):
            clmn = 0
            if i == 0:
                Q[t,:] = Q[t-1,:]
                h[t,:] = h[t-1,:]
            else:
                Q[t,:] = Q[t,:]
                h[t,:] = h[t,:]
        
            if UC == 'Q':
                Q[t][0] = ubc[t]
            else:
                h[t][0] = ubc[t]   
            if DC == 'Q':
                Q[t][M-1] = dbc[t]
            else:
                h[t][M-1] = dbc[t]
                
            for x in range (0, M-1):    
                # continuity equation factors
                A1 = -theta/dx
                B1 = b*(1-Psi)/dt
                C1 = theta/dx
                D1 = b*Psi/dt
                E1 = -(1-theta)*(Q[t-1,x+1]-Q[t-1,x])/dx + b*((1-Psi)*h[t-1,x]/dt+Psi*h[t-1,x+1]/dt)
                # momentum equation factors
                hj_nhalf = 0.5*(h[t,x]+h[t-1,x])
                Aj_nhalf = b*hj_nhalf
                hj1_nhalf = 0.5*(h[t,x+1]+h[t-1,x+1])
                Aj1_nhalf = b*(hj1_nhalf)
                Ajhalf_nhalf = 0.5*( Aj_nhalf+Aj1_nhalf)
                Kj_nhalf = C*b*hj_nhalf**1.5
                Kj1_nhalf = C*b*hj1_nhalf**1.5
                A2 = (1-Psi)/dt - Beta*Q[t-1,x]/(dx*Aj_nhalf) + g*Aj_nhalf*(1-Psi)*abs(Q[t-1,x])/(Kj_nhalf**2.0)
                B2 = -g*Ajhalf_nhalf*theta/dx
                C2 = Psi/dt + Beta*Q[t-1,x]/(dx*Aj_nhalf) + g*Ajhalf_nhalf*(Psi)*abs(Q[t-1,x+1])/(Kj1_nhalf**2.0)
                D2 = theta*g*Ajhalf_nhalf/dx
                E2 = (1-Psi)*Q[t-1,x]/dt + Psi*Q[t-1,x+1]/dt- (1-theta)*g*Ajhalf_nhalf*(h[t-1,x+1]-h[t-1,x])/dx + g*Ajhalf_nhalf*Ib
                F[2*x+1, clmn] = A1
                F[2*x+1, clmn+1] = B1
                F[2*x+1, clmn+2] = C1
                F[2*x+1, clmn+3] = D1
                E[2*x+1, 0] = E1
                F[2*x+2, clmn] = A2
                F[2*x+2, clmn+1] = B2
                F[2*x+2, clmn+2] = C2
                F[2*x+2, clmn+3] = D2
                E[2*x+2, 0] = E2
                clmn = clmn + 2
            if UC == 'Q':
                F[0, 0] = 1
                E[0, 0]= ubc[t]
            else:
                F[0, 1] = 1
                E[0, 0]= ubc[t]  
            if DC == 'Q':
                F[2*M-1,2*M-2] = 1
                E[2*M-1,0]= dbc[t]
            else:
                F[2*M-1,2*M-1] = 1
                E[2*M-1, 0]= dbc[t]
                
            invF = inv(F)
            kn = np.matmul(invF, E)
            x = 0
            y = 0
            for v in range(len(kn)):
                if v%2 ==0:
                    Q[t, x] = kn[v]
                    x = x + 1
                else:
                    h[t, y] = kn[v]
                    y = y + 1
        
    for t in range (0, N):
        for x in range (0, M):        
            WL[t, x] = h[t, x] + (lgth-x*dx)*Ib
    for x in range (0, M): 
        hg[0, x] = (lgth-x*dx)*Ib
    return Q, h, hg, WL

def writefile():
    with open('Discharge.out', 'w') as fname:
        # writing the heading
        fname.write('Tanvir Ahmed, Locker no.: 357 Student no.: 1045031 \n')
        fname.write('Computed Discharge (Q)\n')
        fname.write('Time ')
        for i in range (0, M):
            fname.write('Q_{:d} ' .format(i))
        fname.write('\n')
    
    with open('WaterDepth.out', 'w') as fname:
        # writing the heading
        fname.write('Tanvir Ahmed, Locker no.: 357 Student no.: 1045031 \n')
        fname.write('Computed Water Depth (h)\n')
        fname.write('Time ')
        for i in range (0, M):
            fname.write('h_{:d} ' .format(i))
        fname.write('\n')
        
        with open('Discharge.out', 'a') as fname:
            fname.write('{:4d} ' .format(t))
            for z in range (0, M):
                fname.write('{:4.1f} ' .format(Q[t][z]))
            fname.write('\n')  
       
        with open('WaterDepth.out', 'a') as fname:
            fname.write('{:4d} ' .format(t))
            for z in range (0, M):
                fname.write('{:4.1f} ' .format(h[t][z]))
            fname.write('\n')

